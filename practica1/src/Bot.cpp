#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

#include "DatosMemCompartida.h"

int main ()
{
	DatosMemCompartida *pMemoriaCompartida;

	int fd;
	char *org;
	fd = open("FIFObot.txt",O_RDWR);
	org = (char*)mmap (NULL,sizeof(*(pMemoriaCompartida)),PROT_WRITE|PROT_READ,MAP_SHARED,fd,0);

	close (fd);

	pMemoriaCompartida = (DatosMemCompartida*)org;

	while (1)
	{
		float posiciony = (pMemoriaCompartida->raqueta1.y2+pMemoriaCompartida->raqueta1.y1)/2;//Punto medio entre la parte superior de la raqueta y la inferior

		if (posiciony<pMemoriaCompartida->esfera.centro.y)
			pMemoriaCompartida->accion =1;
		else if (posiciony>pMemoriaCompartida->esfera.centro.y)
			pMemoriaCompartida->accion =-1;
		else pMemoriaCompartida ->accion =0;

		usleep(25000);
	}
	munmap(org,sizeof(*(pMemoriaCompartida)));
}