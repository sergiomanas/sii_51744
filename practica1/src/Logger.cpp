#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

using namespace std;

typedef struct ronda
{
	int jugador;
	int puntos;
};

void imprimePuntos (ronda a)
{
	cout << "El jugador " <<a.jugador<<"ha marcado 1 punto, lleva un total de "<<a.puntos<<"puntos"<<endl;
}

int main(int argc, char **argv)
{
	int fd; //Las pipes son las que tienen dos descriptores de fichero.
	int puntos1, puntos2;
	ronda r;

	if (mkfifo("FIFO.txt",0600)<0)
	{
		perror("No puede crearse el FIFO");
		return (1);
	}

	if ((fd=open ("FIFO.txt",O_RDONLY))<0)
	{
		perror ("No puede abrirse el FIFO");
		return (1);
	}

	while ((read(fd, &r, sizeof (r)))==sizeof(r))
	{
		imprimePuntos(r);
	}

	close (fd);
	unlink ("FIFO.txt");
	return (0);
}